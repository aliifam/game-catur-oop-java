package com.example;

public class Piece {

    private Color color;
    private int positionx;
    private int positiony;


    Piece(Color color, int x, int y) {

        this.color = color;

        this.positionx = 0;
        this.positiony = 0;

        if (1 <= x && x <= 8) {
            this.positionx = x;
        }

        if (1 <= y && y <= 8) {
            this.positiony = y;
        }
    }

    public void setPositionX(int x){
        setPosition(x, this.positiony);
    }

    public void setPositionY(int y){
        setPosition(this.positionx, y);
    }

    public boolean setPosition(int x, int y) {
        if (1 <= x && x <= 8 && 1 <= y && y <= 8) {
            if (1 <= x && x <= 8) {
                this.positionx = x;
            }
            if (1 <= y && y <= 8) {
                this.positiony = y;
            }
            return true;
        } else {
            return false;
        }
    }

    public Color getColor(){
        return this.color;
    }

    public int getPositionX() {
        return this.positionx;
    }

    public int getPositionY() {
        return this.positiony;
    }

    public void move(int x, int y, Piece[][] piece) {
        //IMO properly if using abstract or interface.
    }

    //overloading
    public void move(Piece[][] piece) {
        //IMO properly if using abstract or interface.
    }

    public boolean isValidMove(){
        return true; //for pawn cause not have validation for other overrided
    }
    
}

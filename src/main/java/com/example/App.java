package com.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Game papan = new Game();

        Piece pion = new Pawn(Color.WHITE, 2, 8);
        Piece kuda = new Knight(Color.BLACK, 3, 3);
        Piece benteng = new Rook(Color.WHITE, 8, 8);

        papan.addPiece(pion);
        papan.addPiece(kuda);
        papan.addPiece(benteng);

        papan.printBoard();

        System.out.println();
        System.out.println();

        pion.move(papan.getBoard());
        benteng.move(8, 7, papan.getBoard());
        kuda.move(5, 4, papan.getBoard());

        papan.printBoard();

        System.out.println(pion.getColor().getColor());
        System.out.println(kuda.getColor().getColor());
    }
}

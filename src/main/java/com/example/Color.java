package com.example;

public enum Color {
    WHITE("bidak putih"),
    BLACK("bidak hitam");

    private String color;

    Color(String color){
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}


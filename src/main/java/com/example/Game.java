package com.example;

public class Game {

    private Piece[][] board;

    Game() {
        board = new Piece[8][8];
    }

    public Piece[][] getBoard() {
        return this.board;
    }

    public boolean addPiece(Piece piece) {
        if (board[piece.getPositionX()-1][piece.getPositionY()-1] == null) {
            board[piece.getPositionX()-1][piece.getPositionY()-1] = piece;
            return true;
        } else {
            return false;
        }
    }

    // before
    public void movePiece(Piece piece, int x, int y) { 
        int oldPositionX = piece.getPositionX();
        int oldPositionY = piece.getPositionY();

        if (piece.setPosition(x, y)) {
            board[oldPositionX][oldPositionY] = null;
            board[piece.getPositionX() - 1][piece.getPositionY() - 1] = piece;
        }
    }


    public void printBoard() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(board[j][i] + " | ");
            }
            System.out.println();
        }
    }


}

/**
 * beberapa trik insialisasi array multidimensi java 
 * 
 * int marks[][];           // declare marks array
 * marks = new int[3][5];   // allocate memory for storing 15 elements
 * 
 * int marks[][] = new int[3][5]; //short hands
 */

package com.example;

public class Knight extends Piece {

    Knight(Color color, int x, int y) {
        super(color, x, y);
        //TODO Auto-generated constructor stub
    }
    
    public boolean isValidMove(int stepX, int stepY) {
        if((stepX == 2 || stepX == -2) && (stepY == 1 || stepY == -1)) {
            return true;
        } else if ((stepY == 2 || stepY == -2) && (stepX == 1 || stepX == -1)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean setPosition(int x, int y) {
        if (isValidMove(x - this.getPositionX(), y - this.getPositionY())) {
            return super.setPosition(x, y);
        } else {
            return false;
        }
    }

    public void move(int x, int y, Piece[][] board) {

        int oldPositionX = this.getPositionX();
        int oldPositionY = this.getPositionY();

        if (this.isValidMove(x - oldPositionX, y - oldPositionY)) {
            if (super.setPosition(x, y)) {
                board[oldPositionX - 1][oldPositionY - 1] = null;
                board[this.getPositionX() - 1][this.getPositionY() - 1] = this;
            }
        }
    }

    public String toString() {
        return "KUDA " + this.getColor();
    }
}

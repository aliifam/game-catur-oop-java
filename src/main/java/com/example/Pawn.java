package com.example;

public class Pawn extends Piece {

    Pawn(Color color, int x, int y) {
        super(color, x, y);
        //TODO Auto-generated constructor stub
    }
    
    @Override //just annotation
    public void move(Piece[][] board) {

        int oldPositionY = this.getPositionY();
        int oldPositionX = this.getPositionX();

        if (this.isValidMove()) {
            if (oldPositionY < 8 && oldPositionY > 1) {
                this.setPositionY(oldPositionY + 1);
                board[oldPositionX - 1][oldPositionY - 1] = null;
                board[this.getPositionX() - 1][this.getPositionY() - 1] = this;
            }
        }
    }

    public String toString() {
        return "PION " + this.getColor();
    }
}

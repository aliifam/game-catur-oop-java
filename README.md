# Game Catur OOP Java

Nama : Aliif Arief Maulana
<br>
NIM : 21/479029/SV/19418
<br>
Matkul : Praktikum OOP
<br>
Kelas : PL1AA
<br>
<br>
mengimplementasikan :

- array multidimensi
- polymorphism
- method overriding & overloading
- inheritace
- method overriding
- Maven Quickstart
- Java enum
- Encapsulation

[SOURCE CODE HERE](https://gitlab.com/aliifam/game-catur-oop-java/-/tree/master/src/main/java/com/example)